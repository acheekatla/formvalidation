import React from "react";
import "./style.css";

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      username: "",
      email: "",
      password: "",
      errors: {},
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange = (e) => {
    let targetname = [e.target.name];
    this.setState({ [e.target.name]: e.target.value });
    console.log(targetname[0]);
    console.log(typeof targetname[0]);
    if (targetname[0] === "username") {
      this.setState({ username_error: "" });
    } else if (targetname[0] === "email") {
      this.setState({ email_error: "" });
    } else {
      if (targetname[0] === "password") {
        this.setState({ password_error: "" });
      }
    }
  };

  formValidation = () => {
    const { username, email, password } = this.state;
    let isValid = true;
    const errors = {};
    if (username === "") {
      errors.username = "Please fill this field";
      isValid = false;
      console.log("Please fill this field");
    } else {
      if (!username.match("^(?=[a-zA-Z]{6,9}$)(?!.*[_.]{2})[^_.].*[^_.]$")) {
        errors.username = "username is not valid";
        isValid = false;
      } else {
        errors.usernameLength = "";
        isValid = true;
      }
    }
    if (email === "") {
      errors.email = "Please fill this Field";
      isValid = false;
      console.log("Please fill this field");
    }
    if (password === "") {
      errors.password = "Please fill this Field";
      isValid = false;
    } else {
      if (!password.match("/^(?=.*d)(?=.*[a-z])(?=.*[A-Z]).{8,32}$/")) {
        errors.password =
          "Password must contain a Uppercase,a Lower case, a alphanumeric, a specialcharacter";
        isValid = false;
      } else {
        errors.password = "";
        isValid = true;
      }
    }
    this.setState({ errors });
    return isValid;
  };

  onSubmit = (e) => {
    e.preventDefault();
    const isValid = this.formValidation();
    if (isValid === true) {
      //send username and password to server
      alert("Form Submitted");
      console.log(this.state);
      this.setState({ username: "", email: "", password: "" });
    }
  };
  render() {
    const {username, email, password } = this.state;
    return (
      <>
        {/* main */}
        <div className="main-w3layouts wrapper">
          <h1> SignUp Form</h1>
          <div className="main-agileinfo">
            <div className="agileits-top">
            <form className="agileits-top" id="signupform" onSubmit={this.onSubmit}>
                <label htmlFor="email">username</label>
                <input
                  type="text"
                  name="username"
                  placeholder="Username"
                  value={username}
                  id="username"
                  onChange={this.onChange}
                /><br/>
                <div className="error_message">
                  {this.state.errors.username}
                </div>
                <label htmlFor="email">email</label>
                <input
                  type="text"
                  placeholder="email"
                  name="email"
                  value={email}
                  id="username"
                  onChange={this.onChange}
                />
                <div className="error_message">{this.state.errors.email}</div>

                <br />
                <label htmlFor="password">password</label>
                <input
                  type="password"
                  name="password"
                  value={password}
                  placeholder="Password"
                  id="password"
                  onChange={this.onChange}
                />
                <div className="error_message">
                  {this.state.errors.password}
                </div>
               
                <div className="wthree-text">
                  <label className="anim">
                    <input type="checkbox" className="checkbox" required="" />
                    <span>I Agree To The Terms &amp; Conditions</span>
                  </label>
                  <div className="clear"> </div>
                </div>
                <input type="submit" defaultValue="SIGNUP" />
              </form>
              <p>
                Don't have an Account? <a href="#"> Login Now!</a>
              </p>
            </div>
          </div>

          <div className="colorlibcopy-agile">
            <p>
              © 2018 Colorlib Signup Form. All rights reserved | Design by{" "}
              <a href="https://colorlib.com/" target="_blank">
                Colorlib
              </a>
            </p>
          </div>
          {/* //copyright  */}
          <ul className="colorlib-bubbles">
            <li />
            <li />
            <li />
            <li />
            <li />
            <li />
            <li />
            <li />
            <li />
            <li />
          </ul>
        </div>
        {/* //main */}
      </>
    );
  }
}
export default App;
